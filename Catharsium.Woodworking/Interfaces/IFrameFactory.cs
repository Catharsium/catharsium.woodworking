﻿using Catharsium.Woodworking.Logic.PictureFrames.Models;

namespace Catharsium.Woodworking.Interfaces
{
    public interface IFrameFactory
    {
        Frame MakeMiteredFrame(Canvas canvas, decimal frameWidth);
        Frame MakeHalfLapFrame(Canvas canvas, decimal frameWidth);
    }
}
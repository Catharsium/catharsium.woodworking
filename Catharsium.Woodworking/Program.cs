﻿using System;
using System.IO;
using Catharsium.Woodworking._Configuration;
using Catharsium.Woodworking.Interfaces;
using Catharsium.Woodworking.Logic.PictureFrames.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Catharsium.Woodworking
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, false);
            var configuration = builder.Build();

            var serviceProvider = new ServiceCollection()
                .AddLogging(configure => configure.AddConsole())
                .AddCatharsiumWoodworking(configuration)
                .BuildServiceProvider();

            var frameFactory = serviceProvider.GetService<IFrameFactory>();

            Console.WriteLine("Enter canvas width:");
            var withInput = Console.ReadLine();

            Console.WriteLine("Enter canvas height:");
            var heightInput = Console.ReadLine();

            Console.WriteLine("Enter frame rails/styles width:");
            var frameWidthInput = Console.ReadLine();

            if (!decimal.TryParse(withInput, out var width)) {
                return;
            }

            if (!decimal.TryParse(heightInput, out var height)) {
                return;
            }

            if (!decimal.TryParse(frameWidthInput, out var frameWidth)) {
                return;
            }

            var canvas = new Canvas {
                Width = width,
                Height = height
            };
            var frame = frameFactory.MakeMiteredFrame(canvas, frameWidth);

            Console.WriteLine();
            Console.WriteLine($"Canvas width: {canvas.Width}");
            Console.WriteLine($"Canvas height: {canvas.Height}");
            Console.WriteLine($"Rails length: {frame.Rails}");
            Console.WriteLine($"Styles length: {frame.Styles}");
            Console.WriteLine($"Thickness: {frameWidth}");
        }
    }
}
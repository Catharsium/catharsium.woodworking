﻿using Catharsium.Util.Configuration.Extensions;
using Catharsium.Woodworking.Interfaces;
using Catharsium.Woodworking.Logic.PictureFrames;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Catharsium.Woodworking._Configuration
{
    public static class CatharsiumWoodworkingRegistration
    {
        public static IServiceCollection AddCatharsiumWoodworking(this IServiceCollection services, IConfiguration config)
        {
            var configuration = config.Load<CatharsiumWoodworkingConfiguration>();

            services.AddTransient<IFrameFactory, FrameFactory>();

            return services;
        }
    }
}
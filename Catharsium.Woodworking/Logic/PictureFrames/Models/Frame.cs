﻿namespace Catharsium.Woodworking.Logic.PictureFrames.Models
{
    public class Frame
    {
        public decimal Rails { get; set; }
        public decimal Styles { get; set; }
    }
}
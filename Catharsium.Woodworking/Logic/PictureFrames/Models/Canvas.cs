﻿namespace Catharsium.Woodworking.Logic.PictureFrames.Models
{
    public class Canvas
    {
        public decimal Width { get; set; }
        public decimal Height { get; set; }
    }
}
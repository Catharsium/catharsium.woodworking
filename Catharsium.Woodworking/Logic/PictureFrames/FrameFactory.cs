﻿using Catharsium.Woodworking.Interfaces;
using Catharsium.Woodworking.Logic.PictureFrames.Models;

namespace Catharsium.Woodworking.Logic.PictureFrames
{
    public class FrameFactory : IFrameFactory
    {
        public Frame MakeMiteredFrame(Canvas canvas, decimal frameWidth)
        {
            return new Frame {
                Rails = canvas.Width + 2 * frameWidth,
                Styles = canvas.Height + 2 * frameWidth
            };
        }


        public Frame MakeHalfLapFrame(Canvas canvas, decimal frameWidth)
        {
            return new Frame
            {
                Rails = canvas.Width + 2 * frameWidth,
                Styles = canvas.Height + 2 * frameWidth
            };
        }
    }
}
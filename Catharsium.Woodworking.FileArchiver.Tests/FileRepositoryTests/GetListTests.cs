﻿using System.Linq;
using Catharsium.Util.IO.Interfaces;
using Catharsium.Util.IO.Wrappers;
using Catharsium.Util.Testing;
using Catharsium.Woodworking.FileArchiver.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Catharsium.Woodworking.FileArchiver.Tests.FileRepositoryTests
{
    [TestClass]
    public class GetListTests : TestFixture<FileRepository>
    {
        [TestMethod]
        public void GetList_ReturnsWrappedListOfFilesInPath()
        {
            var directoryPath = "My directory path";
            var files = new IFile[] {new FileInfoWrapper("My file path")};
            var directory = Substitute.For<IDirectory>();
            directory.GetFiles().Returns(files);
            this.GetDependency<IFileFactory>().CreateDirectory(directoryPath).Returns(directory);
            this.SetDependency(directoryPath);

            var actual = this.Target.GetList();
            Assert.AreEqual(files.Length, actual.Count);
            foreach (var episode in actual) {
                Assert.IsTrue(files.Any(f => f.Name == episode.Name));
            }
        }
    }
}
﻿using Catharsium.Util.IO.Interfaces;
using Catharsium.Util.Testing;
using Catharsium.Woodworking.FileArchiver.Models;
using Catharsium.Woodworking.FileArchiver.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Catharsium.Woodworking.FileArchiver.Tests.FileRepositoryTests
{
    [TestClass]
    public class AddTests : TestFixture<FileRepository>
    {
        [TestMethod]
        public void Add_CreatesNewFile()
        {
            var path = "My path";
            this.SetDependency(path);
            var directory = Substitute.For<IDirectory>();
            this.GetDependency<IFileFactory>().CreateDirectory(path).Returns(directory);

            var file = Substitute.For<IFile>();
            var episode = new Episode {Name = "My name"};
            
            this.Target.Add(episode);
           // file.Received().Create();
        }
    }
}
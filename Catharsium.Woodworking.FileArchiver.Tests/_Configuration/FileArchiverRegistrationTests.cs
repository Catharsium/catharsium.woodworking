﻿using Catharsium.Util.IO.Interfaces;
using Catharsium.Util.Testing.Extensions;
using Catharsium.Woodworking.FileArchiver._Configuration;
using Catharsium.Woodworking.FileArchiver.Interfaces;
using Catharsium.Woodworking.FileArchiver.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Catharsium.Woodworking.FileArchiver.Tests._Configuration
{
    [TestClass]
    public class FileArchiverRegistrationTests
    {
        [TestMethod]
        public void AddDocumentGenerator_RegistersDependencies()
        {
            var services = Substitute.For<IServiceCollection>();
            var configuration = Substitute.For<IConfiguration>();

            services.AddFileArchiver(configuration);
            services.ReceivedRegistration<IFileRepositoryFactory, FileRepositoryFactory>();
        }


        [TestMethod]
        public void AddDocumentGenerator_RegistersPackages()
        {
            var services = Substitute.For<IServiceCollection>();
            var configuration = Substitute.For<IConfiguration>();

            services.AddFileArchiver(configuration);
            services.ReceivedRegistration<IFileFactory>();
        }
    }
}
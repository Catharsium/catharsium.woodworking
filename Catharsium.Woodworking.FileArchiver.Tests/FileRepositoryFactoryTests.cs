﻿using Catharsium.Util.IO.Interfaces;
using Catharsium.Util.Testing;
using Catharsium.Woodworking.FileArchiver.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Catharsium.Woodworking.FileArchiver.Tests
{
    [TestClass]
    public class FileRepositoryFactoryTests : TestFixture<FileRepositoryFactory>
    {
        [TestMethod]
        public void Create()
        {
            var folderPath = "My folder path";
            var actual = this.Target.Create(folderPath);
            Assert.IsNotNull(actual);
            this.GetDependency<IFileFactory>().CreateDirectory(folderPath);
        }
    }
}
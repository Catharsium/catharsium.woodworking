﻿using Catharsium.Util.Testing.Extensions;
using Catharsium.Woodworking._Configuration;
using Catharsium.Woodworking.Interfaces;
using Catharsium.Woodworking.Logic.PictureFrames;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace Catharsium.Woodworking.Tests._Configuration
{
    [TestClass]
    public class CatharsiumWoodworkingRegistrationTests
    {
        [TestMethod]
        public void AddCatharsiumUtilities_RegistersDependencies()
        {
            var serviceCollection = Substitute.For<IServiceCollection>();
            var config = Substitute.For<IConfiguration>();

            serviceCollection.AddCatharsiumWoodworking(config);
            serviceCollection.ReceivedRegistration<IFrameFactory, FrameFactory>();
        }
    }
}
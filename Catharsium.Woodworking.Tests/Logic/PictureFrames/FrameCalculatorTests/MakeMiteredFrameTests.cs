﻿using Catharsium.Util.Testing;
using Catharsium.Woodworking.Logic.PictureFrames;
using Catharsium.Woodworking.Logic.PictureFrames.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Catharsium.Woodworking.Tests.Logic.PictureFrames.FrameCalculatorTests
{
    [TestClass]
    public class MakeMiteredFrameTests : TestFixture<FrameFactory>
    {
        [TestMethod]
        public void MakeMiteredFrame_ValidCanvasAndFrameWidth_ReturnsFittingFrame()
        {
            var frameWidth = 2;
            var canvas = new Canvas {
                Width = 5,
                Height = 4
            };

            var actual = this.Target.MakeMiteredFrame(canvas, frameWidth);
            Assert.AreEqual(canvas.Width + frameWidth + frameWidth, actual.Rails);
            Assert.AreEqual(canvas.Height + frameWidth + frameWidth, actual.Styles);
        }
    }
}
﻿using Catharsium.Util.IO._Configuration;
using Catharsium.Woodworking.FileArchiver.Interfaces;
using Catharsium.Woodworking.FileArchiver.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Catharsium.Woodworking.FileArchiver._Configuration
{
    public static class FileArchiverRegistration
    {
        public static IServiceCollection AddFileArchiver(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IFileRepositoryFactory, FileRepositoryFactory>();

            services.AddIoUtilities(configuration);

            return services;
        }
    }
}
﻿namespace Catharsium.Woodworking.FileArchiver.Models
{
    public class Episode
    {
        public string Series { get; set; }
        public int Number { get; set; }
        public string Name { get; set; }
    }
}
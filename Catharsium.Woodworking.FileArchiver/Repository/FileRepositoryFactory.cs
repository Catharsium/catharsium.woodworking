﻿using Catharsium.Util.IO.Interfaces;
using Catharsium.Woodworking.FileArchiver.Interfaces;

namespace Catharsium.Woodworking.FileArchiver.Repository
{
    public class FileRepositoryFactory : IFileRepositoryFactory
    {
        private readonly IFileFactory fileFactory;


        public FileRepositoryFactory(IFileFactory fileFactory)
        {
            this.fileFactory = fileFactory;
        }


        public IFileRepository Create(string folder)
        {
            return new FileRepository(this.fileFactory, folder);
        }
    }
}
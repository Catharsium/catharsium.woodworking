﻿using Catharsium.Util.IO.Interfaces;
using Catharsium.Woodworking.FileArchiver.Interfaces;
using Catharsium.Woodworking.FileArchiver.Models;
using System.Collections.Generic;
using System.Linq;

namespace Catharsium.Woodworking.FileArchiver.Repository
{
    public class FileRepository : IFileRepository
    {
        private readonly IFileFactory fileFactory;
        private readonly string folder;

        public IDirectory Directory { get; set; }


        public FileRepository(IFileFactory fileFactory, string folder)
        {
            this.fileFactory = fileFactory;
            this.folder = folder;
            this.Directory = fileFactory.CreateDirectory(folder);
        }



        public List<Episode> GetList()
        {
            return this.Directory.GetFiles()
                .Select(f => new Episode {Name = f.Name})
                .ToList();
        }


        public void Add(Episode episode)
        {
            var file = this.fileFactory.CreateFile($"{this.folder}/{episode.Name}");
            file.Create();
        }
    }
}
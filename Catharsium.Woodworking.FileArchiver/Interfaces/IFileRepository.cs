﻿using System.Collections.Generic;
using Catharsium.Util.IO.Interfaces;
using Catharsium.Woodworking.FileArchiver.Models;

namespace Catharsium.Woodworking.FileArchiver.Interfaces
{
    public interface IFileRepository
    {
        IDirectory Directory { get; set; }

        List<Episode> GetList();

        void Add(Episode episode);
    }
}
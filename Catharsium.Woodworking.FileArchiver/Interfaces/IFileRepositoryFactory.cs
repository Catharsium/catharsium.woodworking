﻿namespace Catharsium.Woodworking.FileArchiver.Interfaces
{
    public interface IFileRepositoryFactory
    {
        IFileRepository Create(string folder);
    }
}